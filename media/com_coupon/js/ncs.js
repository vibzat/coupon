var ncs = {
   cLs: '',
   pdn: 0,
   pdc: 0,
   oCC: '',
   oUL: true,
   oUC: '',
   oUCD: false,

   mCode: function(cb){
      var frm = cb.form;
      if(cb.checked){
         this.oCC = $('coupon_code').get('value');
         $('coupon_code').set('value', '');
         $('coupon_code').set('disabled', 'disabled');
      }
      else{
         $('coupon_code').set('value', this.oCC);
         $('coupon_code').removeProperty('disabled');
      }
   },

   addRC: function(f){
      var ct = document.getElementById('coupon_type');
      var dv = document.getElementById('section-rewards');
      if($('coupon_type').get('value') == 'campaign'){
         f.value = label.rcr;
         $('coupon_type').set('value', 'rewards');
         $('section-rewards').setStyle('display', 'inline');
      }
      else{
         f.value = label['acr'];
         $('coupon_type').set('value', 'campaign');
         $('section-rewards').setStyle('display', 'none');
      }
   }
}