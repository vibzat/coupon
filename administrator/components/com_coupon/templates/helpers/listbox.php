<?php

class ComCouponTemplateHelperListbox extends ComDefaultTemplateHelperListbox {
   public function templates($config = array()){
      $config = new KConfig($config);

      $options[] = $this->option(array('text' => JText::_('coupon'), 'value' => 'coupon'));
      $options[] = $this->option(array('text' => JText::_('free_product'), 'value' => 'free_product'));
      $options[] = $this->option(array('text' => JText::_('voucher'), 'value' => 'voucher'));
      $list = $this->optionlist(array(
         'options'   => $options,
         'name'      => $config->name,
         'selected'  => $config->{$config->name},
         'attribs'   => $config->attribs
      ));

      return $list;
   }
}
?>
