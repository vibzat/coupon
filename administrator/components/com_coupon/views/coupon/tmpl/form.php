<?php
defined('KOOWA') or die('Restricted access');

$label['unltd'] = @text('Unlimited');
$calendar_behavior = 'com://admin/articles.template.helper.behavior.calendar';
?>

<?= @helper('behavior.tooltip') ?>
<?= @helper('behavior.validator') ?>
  
<script src="media://lib_koowa/js/koowa.js" />
<style src="media://lib_koowa/css/koowa.css" />
<script src="media://com_coupon/js/ncs.js" />
<style src="media://com_coupon/css/coupon.css" />
<script>
var label = {
   acr: '<?= @text('ADD REWARDS CRITERIA') ?>',
   rcr: '<?= @text('REMOVE REWARDS CRITERIA') ?>',
   da: '<?= @text('ADVANCED DISCOUNT') ?>',
   db: '<?= @text('BASIC DISCOUNT') ?>',
   ema: '<?= @text('Please enter the Minimum Amount Purchased.') ?>',
   emd: '<?= @text('Please enter the Discount Value.') ?>',
   efp: '<?= @text('Please enter the Free Product Quantity') ?>',
   cap: '<?= @text('All Products') ?>',
   cac: '<?= @text('All Customers') ?>',
   caca: '<?= @text('All Categories') ?>',
   cag: '<?= @text('All Groups') ?>',
   aap: '<?= @text('applicable products') ?>',
   aac: '<?= @text('applicable customers') ?>',
   aaca: '<?= @text('applicable categories') ?>',
   none: '<?= @text('None') ?>',
   del: '<?= @text('delete') ?>',
   edit: '<?= @text('Edit') ?>',
   mvdn: '<?= @text('Move Down') ?>',
   mvup: '<?= @text('Move Up') ?>',
   adddsc: '<?= @text('ADD DISCOUNT') ?>',
   upddsc: '<?= @text('UPDATE DISCOUNT') ?>',
   unltd: '<?= $unltd ?>'
}
</script>

<form action="" method="post" id="coupon-form" class="-koowa-form">
   <input type="hidden" name="id" value="<?= $coupon->id; ?>" />
   <input type="hidden" name="coupon_type" id="coupon_type" value="<?= $coupon->coupon_type ?>" />

   <div style="width:100%; float: left" id="mainform">
      <fieldset class="adminform">
         <legend><?= @text('Details'); ?></legend>
         <div id="addedit_form" style="margin-bottom:15px">

            <div class="label"><?= @text( 'Title' ); ?></div>
            <div class="field">
               <input class="inputbox" type="text" name="title" id="title" size="60" maxlength="255" value="<?= $coupon->title; ?>" />
            </div>
            <div class="clr"></div>

            <div class="label"><?= @text( 'Customer Info Box' ); ?></div>
            <div class="field">
               <textarea name="info" id="info" rows="3" cols="45" class="inputbox"><?= $coupon->info; ?></textarea>
               <br />(<?= @text('enter message here if you want customer to see when they enter this coupon in') ?>)
            </div>
            <div class="clr"></div>

            <div class="label"><?= @text( 'Template' ); ?></div>
            <div class="field"><?= @helper('listbox.templates', array('name' => 'template', 'selected' => $coupon->template)); ?></div>
            <div class="clr"></div>

            <div class="label"><?= @text( 'Coupon Code' ) ?></div>
            <div class="field"><input name="coupon_code" type="text" class="inputbox" id="coupon_code" size="15" maxlength="15" value="<?= $coupon->coupon_code ?>" /></div>
            <div class="clr"></div>

            <div class="label"></div>
            <div class="field">
               <input type="checkbox" name="multiple_codes" id="multiple_codes" onclick="ncs.mCode(this)" value="1"<?php echo @$coupon->multiple_codes ? ' checked' : '';?>  />
               <?= @text('Create multiple single use coupon codes.') ?>
            </div>
            <div class="clr"></div>

            <div class="label"><?= @text('Coupon Validity') ?></div>
            <div class="field">
               <?= @helper($calendar_behavior, array('date' => $coupon->start_date, 'name' => 'start_date', 'attribs' => array('size' => 10), 'format' => '%Y-%m-%d')); ?>
               &nbsp; &nbsp;
               <?= @helper($calendar_behavior, array('date' => $coupon->end_date, 'name' => 'end_date', 'attribs' => array('size' => 10), 'format' => '%Y-%m-%d')); ?>
            </div>
            <div class="clr"></div>

            <div class="label"></div>
            <div class="field"><br /><input type="button" value="<?= @text('ADD REWARDS CRITERIA') ?>" onclick="ncs.addRC(this)" /></div>
            <div class="clr"></div>

            <div id="section-rewards" style="display:none;">
               <fieldset class="adminform" style="width:750px">
                  <legend><?= @text('Reward Qualification Criteria') ?></legend>

                  <div class="label"><?= @text('Minimum Purchased Amount') ?></div>
                  <div class="field">
                     <input name="reward_amount" type="text" class="inputbox" id="reward_amount" value="<?= $coupon->reward_amount ?>" size="10" />
                  </div>
                  <div class="clr"></div>

                  <div class="label"><?= @text('Promo Period') ?></div>
                  <div class="field">
                     <?= @helper($calendar_behavior, array('date' => $coupon->reward_sdate, 'name' => 'reward_sdate', 'attribs' => array('size' => 10), 'format' => '%Y-%m-%d')); ?>
                     &nbsp; &nbsp;
                     <?= @helper($calendar_behavior, array('date' => $coupon->reward_edate, 'name' => 'reward_edate', 'attribs' => array('size' => 10), 'format' => '%Y-%m-%d')); ?>
                  </div>
                  <div class="clr"></div>
                  <br />
               </fieldset>
            </div>

         </div>
      </fieldset>
   </div>

</form>
