<?php defined('KOOWA') or die; ?>

<form action="<?= @route()?>" method="get" class="-koowa-grid">
    <table class="adminlist"  style="clear: both;">
        <thead>
            <tr>
                <th width="20"></th>
                <th width="20"></th>
                <th width="100"><?= @text('Coupon Code') ?></th>
                <th>
                    <?= @helper('grid.sort', array('column'=>'title'))?>
                </th>
                <th width="100">
                    <?= @helper('grid.sort', array('title'=>'From Date', 'column'=>'start_date'))?>
                </th>
                <th width="100">
                    <?= @helper('grid.sort', array('title'=>'To Date', 'column'=>'end_date'))?>
                </th>
                <th width="80">
                  <?= @text('Discount') ?>
                </th>
                <th width="60">
                  <?= @text('Uses Limit') ?>
                </th>
                <th width="50">
                  <?= @text('Uses') ?>
                </th>
                <th width="20">
                  <?= @text('Mail') ?>
                </th>
                <th width="120">
                    <?= @helper('grid.sort', array('title'=>'Modified On', 'column'=>'modified_on'))?>
                </th>
            </tr>
            <tr>
                <td align="center">
                    <?= @helper( 'grid.checkall'); ?>
                </td>
                <td> </td>
                <td> </td>
                <td>
                    <?=@helper('grid.search');?> 
                </td>
                <td> </td>
                <td> </td>
                <td> </td>
                <td> </td>
                <td> </td>
                <td> </td>
                <td> </td>
            </tr>
        </thead>

       <tbody>
            <?= @template('default_list')?>

            <? if (!count($coupons)): ?>
            <tr>
                <td colspan="20" align="center">
                    <?= @text('No items found'); ?>
                </td>
            </tr>
            <? endif; ?>

        </tbody>

        <tfoot>
            <tr>
                <td colspan="20">
                    <?= @helper('paginator.pagination', array('total' => $total)) ?>
                </td>
            </tr>
        </tfoot>
    </table>
</form>
