<?php defined('KOOWA') or die; ?>

<? $i = 0;?>
<? foreach ($coupons as $coupon): ?>
<tr>
    <td align="center">
        <?= @helper('grid.checkbox', array('row'=>$coupon)); ?>
    </td>
    <td align="center">
      <img src="media://com_coupon/images/document-pencil.png" />
    </td>
    <td align="left">
      <?= $coupon->coupon_code ?>
    </td>
    <td align="left">
      <?= $coupon->title ?>
    </td>
    <td align="center">
      <?= $coupon->start_date == '0000-00-00' ? '--' : $coupon->start_date ?>
    </td>
    <td align="center">
      <?= $coupon->end_date == '0000-00-00' ? '--' : $coupon->end_date ?>
    </td>
    <td align="right">
      <?= $coupon->discount_type == '%' ? $coupon->discount . '%' : '$ ' . $coupon->discount ?>
    </td>
    <td align="center">
      <?= $coupon->uses ? $coupon->uses : @text('Unlimited') ?>
    </td>
    <td align="center">
      <?= $coupon->uses_count ?>
    </td>
    <td>
      <img src="media://com_coupon/images/mail.png" />
    </td>
    <td align="left">
       <?=@helper('date.humanize', array('date' => $coupon->modified_on)); ?>
    </td>
</tr>
<? ++$i?>
<? endforeach; ?>