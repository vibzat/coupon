<?php

class ComCouponModelCoupons extends ComDefaultModelDefault
{
   public function __construct(KConfig $config)
   {
      parent::__construct($config);

      $this->_state
         ->insert('title', 'string')
         ->insert('start_date', 'date')
         ->insert('end_date', 'date')
         ->insert('date', 'date');
   }

   public function getCoupons()
   {
        $database = $this->getTable()->getDatabase();

      $query = $database->getQuery()
         ->distinct()
         ->from('coupon_coupons AS tbl')
         ->order('date')
         ->group('coupon_coupon_id');

      $this->_buildQueryWhere($query);

      $result = $database->select($query, KDatabase::FETCH_FIELD_LIST);
      
      return $result;
   }

   protected function _buildQueryColumns(KDatabaseQuery $query)
   {
      parent::_buildQueryColumns($query);

      $query->select('cc.coupon_code AS coupon_code')
         ->select('cd.discount AS discount')
         ->select('cd.discount_type AS discount_type');
   }

   protected function _buildQueryJoins(KDatabaseQuery $query)
   {
      parent::_buildQueryJoins($query);

      $query->join('LEFT', 'coupon_codes AS cc', 'cc.coupon_coupon_id = tbl.coupon_coupon_id')
         ->join('LEFT', 'coupon_discounts AS cd', 'cd.coupon_coupon_id = tbl.coupon_coupon_id');
   }


   protected function _buildQueryWhere(KDatabaseQuery $query)
   {
      $state = $this->_state;

      if ($this->_state->title) {
         $query->where('tbl.title', '=', $state->title);
      }

      if($state->search)
      {
         $search = '%'.$state->search.'%';
         $query->where('title', 'LIKE',  $search)
            ->where('info', 'LIKE', $search, 'OR');
      }

      parent::_buildQueryWhere($query);
   }
}  
