<?php
defined('KOOWA') or die;

class ComCouponDispatcher extends ComDefaultDispatcher
{
    protected function _initialize(KConfig $config)
    {
        $config->append(array(
            'controller' => 'coupons'
        ));

        parent::_initialize($config);
    }
}
